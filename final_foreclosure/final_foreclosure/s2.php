<?php

include('dbconnection.php');
//index.php
//$connect = mysqli_connect("dallas142", "jtrust08_auction", "abc1234", "jtrust08_auction");
$query = "SELECT distinct auctioncity FROM auction_master ";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
 <head>
  <title>How to return JSON Data from PHP Script using Ajax Jquery</title>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>!-->
  <!-- Price range slider!-->
  <script type="text/javascript">

  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 50000,
      values: [ 0, 50000 ],
      slide: function( event, ui ) {
        console.log(ui.values);
        $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		$( "#amount1" ).val(ui.values[ 0 ]);
		$( "#amount2" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
     " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });

  </script>
  <!-- from Datepicker!-->
  <script>
  $( function() {
    var fromdate=$( "#datepickerfrom", ).datepicker({
      //buttonImage: "datepicker.gif",
      dateFormat:"yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
  } );
  </script>
  <!-- to Datepicker!-->
  <script>
  $( function() {
    var todate=$( "#datepickerto" ).datepicker({
      dateFormat:"yy-mm-dd",
      changeMonth: true,
      changeYear: true
    });
  } );
  </script>

   </head>
 <body>
  <br /><br />
  <div class="container" id="searchauc" style="width:900px;">

   <h3 align="center">Search Auctions</h3><br />
   <div class="row">
    <div class="col-md-4">
    CITY: <select name="citylist" id="citylist" class="form-control">
      <option value="">Select city </option>
      <?php
      while($row = mysqli_fetch_array($result))
      {
       echo '<option value="'.$row["auctioncity"].'">'.$row["auctioncity"].'</option>';

}
?>
</select>
<br>
BANK:<select name="banklist" id="banklist" class="form-control">
 <option value="">Select Bank </option>
 <?php
 $query1 = "SELECT distinct bankid FROM auction_master ";
 $result1 = mysqli_query($connect, $query1);
 while($row = mysqli_fetch_array($result1))
 {
  echo '<option value="'.$row["bankid"].'">'.$row["bankid"].'</option>';

}
?>
</select>

<p>
   Price Range:<p id="amount"></p>
 </p>

 <div id="slider-range"></div>


   <input type="hidden" name="amount1" id="amount1">
   <input type="hidden" id="amount2" name="amount2">

   <p>From: <input type="text" id="datepickerfrom"></p>
   <p>To: <input type="text" id="datepickerto"></p>



   </div>

   <br/>

   <div class="col-md-4">

    <button type="button" name="search" id="search" class="btn btn-info">Search</button>
    <button type="button" name="reset" id="reset" class="btn btn-info">Reset</button>
   </div>
  </div>
 </body>
</html>

<div id="mydisplay">
  <br>
  <div class="w3-container" id="myTable">
<div class="w3-card-4" style="width:25%">
<header class="w3-container w3-light-grey">
<a href="auctionprofile.php?auctionid='.$row["auctionid"].'"> <p id="auctionid">Auction Id: </p></a>
  </header>
  <div class="w3-container">
  Bank: <p id="bankid"></p>
  <p id="minprice"></p>
  <p id="fromdate"></p>
  <p id="todate"></p>
  </div>
</div>
</div>

</div>

<script>

$('#mydisplay').hide();
$('#reset').click(function(){
  $('#searchauc').empty();
});
$(document).ready(function(){
 $('#search').click(function(){
  var city= $('#citylist').val();
  var bankid= $('#banklist').val();
  var minprice= $('#amount1').val();
  var maxprice= $('#amount2').val();
  var fromdate=$('#datepickerfrom').val();
  var todate=$('#datepickerto').val();


  if(city!= ''|| bankid!='')
  {
    $('#mydisplay').show();
   $.ajax({

    url:"fetch.php",
    method:"POST",
    data:{'city':city,'bankid':bankid,'minprice':minprice,'maxprice':maxprice,'fromdate':fromdate,'todate':todate},
    dataType:"JSON",
    success:function(data)
    {
      console.log(data);
        console.log($query);
     $('#auctionid').text(data.auctionid);
     $('#bankid').text(data.bankid);
     $('#minprice').text(data.minprice);
     $('#fromdate').text(data.fromdate);
     $('#todate').text(data.todate);
     if(data.length==0)
     {
       $('#mydisplay').hide();
     }



   }

   })
  }
  else
  {
   alert("Please Select Employee");
   $('#auction_details').css("display", "none");
  }
 });
});
</script>




<?php include('footer.php'); ?>
