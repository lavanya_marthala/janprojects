<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<br>
<div class="container_premium">

  <div class="panel-group" id="accordion_premium">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_premium" href="#premiumsearch1">Search</a>
        </h4>
      </div>
      <div id="premiumsearch1" class="panel-collapse collapse   ">
        <div class="panel-body"><?php include 'user_features/premiumsearch.php';?></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_premium" href="#premiumsearch2">Slider</a>
        </h4>
      </div>
      <div id="premiumsearch2" class="panel-collapse collapse   ">
        <div class="panel-body"><?php include 'user_features/premiumslider.php';?></div>
      </div>
    </div>




  </div>
</div>

</body>
</html>
