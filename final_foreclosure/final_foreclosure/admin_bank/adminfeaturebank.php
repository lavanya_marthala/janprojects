
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<br>
<div class="container_bank">

  <div class="panel-group" id="accordion_bank">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_bank" href="#featurebanksearch">Search</a>
        </h4>
      </div>
      <div id="featurebanksearch" class="panel-collapse collapse   ">
        <div class="panel-body"><?php include 'admin_bank/adminfeaturebanksearch.php';?></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_bank" href="#bankdetails">Bank edit and delete</a>
        </h4>
      </div>
      <div id="bankdetails" class="panel-collapse collapse ">
        <div class="panel-body"><?php include 'admin_bank/adminfeaturebankedit.php';?></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_bank" href="#addbanks">Add Banks</a>
        </h4>
      </div>
      <div id="addbanks" class="panel-collapse collapse ">
        <div class="panel-body"><?php include 'admin_bank/adminfeaturebankadd.php';?></div>
      </div>
    </div>

  </div>
</div>

</body>
</html>
