
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<br>
<div class="container_auction">

  <div class="panel-group" id="accordion_auction">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_auction" href="#featureauctionsearch">Search Auctions</a>
        </h4>
      </div>
      <div id="featureauctionsearch" class="panel-collapse collapse   ">
        <div class="panel-body"><?php include 'admin_auction/adminfeatureauctionsearch.php';?></div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_auction" href="#approveauctions">Approve Auctions</a>
        </h4>
      </div>
      <div id="approveauctions" class="panel-collapse collapse ">
        <div class="panel-body"><?php include 'admin_auction/adminfeatureauctionapprove.php';?></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_auction" href="#editdetails">Auction edit and delete</a>
        </h4>
      </div>
      <div id="editdetails" class="panel-collapse collapse ">
        <div class="panel-body"><?php include 'admin_auction/adminfeatureauctionedit.php';?></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_auction" href="#addauctions">Add Auctions</a>
        </h4>
      </div>
      <div id="addauctions" class="panel-collapse collapse ">
        <div class="panel-body"><?php include 'admin_auction/adminfeatureauctionadd.php';?></div>
      </div>
    </div>



  </div>
</div>

</body>
</html>
