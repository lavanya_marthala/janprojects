<?php include("header.php") ?>
<html>
<head>
  <style>
  .input-group-addon,
input[type=email],
input[type=submit],
.modal-content{border-radius:0px}
</style>
</head>

<body>
  <div class="stats">
  <div class="container">
    <h2>Stats</h2>
    <div class="panel panel-default">
      <div class="panel-body"><?php include ("displaystats.php");?></div>
    </div>
  </div>
  </div>

<div class="latestauctions">
<div class="container">
  <h2>Latest Autions</h2>
  <div class="panel panel-default">
    <div class="panel-body"><?php include ("displaylatestauctions.php");?></div>
  </div>
</div>
</div>

<div class="subscribe">
  <div class="container">
    <div class="panel panel-default">
      <div class="panel-body">

<p class="flex-containe">
    <a role="button" href="#" data-toggle="modal" class="btn btn-lg btn-primary" data-target="#myModal" role="button">Subscribe</a>
</p>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-smll" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h2 class="modal-title" id="myModalLabel">Subscribe to our Newsletter.</h2>
                <p>We promise we will not spam you.</p>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="maillist.php">
                        <div class="form-group col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
                            <input type="email" name ="email" id="email"class=" form-control input-lg"  placeholder="Your email here">
                        </div>
                    </div>
                     <div class="form-group col-md-12">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary btn-lg btn-block" value="Subscribe">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>




</body>
</html>
<?php include("footer.php") ?>
