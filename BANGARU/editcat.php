
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    $('#primary').on('change',function(){
        var primaryID = $(this).val();
        //alert(primaryID);
        if(primaryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'p_id='+primaryID,
                success:function(html){
                    $('#secondary').html(html);
                    $('#tertiary').html('<option value="">Select secondary first</option>'); 
                }
            }); 
        }else{
            $('#secondary').html('<option value="">Select primary first</option>');
            $('#tertiary').html('<option value="">Select secondary first</option>'); 
        }
    });
    
    $('#secondary').on('change',function(){
        var secondaryID = $(this).val();
        if(secondaryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'s_id='+secondaryID,
                success:function(html){
                    $('#tertiary').html(html);
                }
            }); 
        }else{
            $('#tertiary').html('<option value="">Select secondary first</option>'); 
        }
    });
});
</script>

<?php
  
    include 'dbConfig.php';
    $query = $db->query("SELECT * FROM cat1 where status=1 ORDER BY p_id ASC");
    $rowCount = $query->num_rows;
    ?>

    <b>Primary Category :</b>
    <select id="primary">
        <option value="">Select Primary</option>
        <?php
        if($rowCount > 0){
            while($row = $query->fetch_assoc()){ 
                echo '<option value="'.$row['p_id'].'">'.$row['Primarycat'].'</option>';
            }
        }else{
            echo '<option value="">Primarycat not available</option>';
        }
        ?>
    </select><br><br>

    <b> Secondary Category :</b>
    <select id="secondary">
        <option value="">Select primary first</option>
    </select><br><br>

    <b> Tertiary Category :</b>
    <select id="tertiary">
        <option value="">Select secondary first</option>
    </select><br><br>

<b > Link :</b>
       <input type="text" name="link">
       <input type="submit" value="Delete">
       <input type="submit" value="Update">