<?php
//Include the database configuration file
include 'dbConfig.php';

if(!empty($_POST["p_id"])){
    //Fetch all state data
    $query = $db->query("SELECT * from cat2 WHERE p_id = ".$_POST['p_id']." and status=1 ORDER BY Secondarycat ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //State option list
    if($rowCount > 0){
        echo '<option value="">Select secondary</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['s_id'].'">'.$row['Secondarycat'].'</option>';
        }
    }else{
        echo '<option value="">Secondary not available</option>';
    }
}elseif(!empty($_POST["s_id"])){
    //Fetch all city data
    $query = $db->query("SELECT * FROM cat3 WHERE s_id = ".$_POST['s_id']." and status=1 ORDER BY Tertiarycat ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //City option list
    if($rowCount > 0){
        echo '<option value="">Select tertiary</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['t_id'].'">'.$row['Tertiarycat'].'</option>';
        }
    }else{
        echo '<option value="">tertiary not available</option>';
    }
}
?>