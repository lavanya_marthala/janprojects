-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 07, 2018 at 07:16 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cat1`
--

DROP TABLE IF EXISTS `cat1`;
CREATE TABLE IF NOT EXISTS `cat1` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `Primarycat` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active',
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cat1`
--

INSERT INTO `cat1` (`p_id`, `Primarycat`, `status`) VALUES
(1, 'L1 A', 1),
(2, 'L1 B', 1),
(3, 'L1 C', 1),
(4, 'L1 D', 1),
(5, 'L1 E', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cat2`
--

DROP TABLE IF EXISTS `cat2`;
CREATE TABLE IF NOT EXISTS `cat2` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `Secondarycat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `p_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active',
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cat2`
--

INSERT INTO `cat2` (`s_id`, `Secondarycat`, `p_id`, `status`) VALUES
(1, 'L2 A', 1, 1),
(2, 'L2 B', 2, 1),
(3, 'L2 C', 3, 1),
(4, 'L2 D', 4, 1),
(5, 'L2 E', 5, 1),
(6, 'L2 Z', 1, 1),
(7, 'L2 W', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cat3`
--

DROP TABLE IF EXISTS `cat3`;
CREATE TABLE IF NOT EXISTS `cat3` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `Tertiarycat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `s_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active',
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cat3`
--

INSERT INTO `cat3` (`t_id`, `Tertiarycat`, `s_id`, `status`) VALUES
(3, 'L3 l', 1, 1),
(5, 'L3 Q', 1, 1),
(6, 'L3 F', 1, 1),
(7, 'L3 G', 3, 1),
(8, 'L3 H', 3, 1),
(9, 'L3 I', 3, 1),
(10, 'L3 J', 7, 1),
(11, 'L3 K', 7, 1),
(12, 'L3 L', 7, 1),
(13, 'L3 M', 4, 1),
(14, 'L3 N', 3, 1),
(15, 'L3 O', 2, 1),
(16, 'L3 P', 1, 1),
(17, 'L3 Q', 7, 1),
(18, 'L3 R', 7, 1),
(19, 'L3 S', 7, 1),
(20, 'L3 T', 6, 1),
(21, 'L3 U', 6, 1),
(22, 'L3 V', 6, 1),
(23, 'L3 W', 4, 1),
(24, 'L3 X', 4, 1),
(25, 'L3 Y', 2, 1),
(26, 'L3 Z', 2, 1),
(27, 'L3 A', 12, 1),
(28, 'L3 B', 4, 1),
(29, 'L3 C', 14, 1),
(30, 'L3 D', 14, 1),
(31, 'L3 E', 14, 1),
(32, 'L3 F', 15, 1),
(33, 'L3 G', 15, 1),
(34, 'L3 H', 15, 1),
(38, 'LF 5', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
CREATE TABLE IF NOT EXISTS `mail` (
  `Email` varchar(50) NOT NULL,
  `Flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:sunsubscribe 1:subscribe',
  PRIMARY KEY (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mail`
--

INSERT INTO `mail` (`Email`, `Flag`) VALUES
('kk@gmail.com', 1),
('sinzith.tatikonda@chidhagni.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `created`, `modified`, `status`, `admin`) VALUES
(4, 'admin', 'a', 'admin@chidhagni.com', '81dc9bdb52d04dc20036dbd8313ed055', '9999999999', '2018-01-21 14:23:31', '2018-01-21 14:23:31', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `ID` bigint(255) NOT NULL AUTO_INCREMENT,
  `LINK` varchar(500) NOT NULL,
  `Primarycat` varchar(500) DEFAULT NULL,
  `Secondarycat` varchar(500) DEFAULT NULL,
  `Tertiarycat` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`ID`, `LINK`, `Primarycat`, `Secondarycat`, `Tertiarycat`) VALUES
(22, 'lZmZzHcDLwg', 'L1 E ', 'L2 C', 'L3 C'),
(21, 'Rt2k-ImLZH4', 'L1 A', 'L2 B', 'L3 D'),
(20, 'MG2K384EWA0', 'L1 E ', 'L2 E', 'L3 B'),
(19, 'UQbpsKcif0g', 'L1 E ', 'L2 D', 'L3 E'),
(18, 'CBasP_NNyIw', 'L1 D', 'L2 C', 'L3 E'),
(17, 'Qjzn45JE7jA', 'L1 D', 'L2 B', 'L3 E'),
(16, 'Um_4ZZY7a3o', 'L1 C', 'L2 A', 'L3 E'),
(15, '_Mt4cfx5jUA', 'L1 C', 'L2 E', 'L3 D'),
(14, 'bSHNcn_qwrQ', 'L1 B', 'L2 E', 'L3 D'),
(13, 'dvwES_bvs8A', 'L1 B', 'L2 E', 'L3 D'),
(12, 'BChp647ZZ3Q', 'L1 A ', 'L2 D', 'L3 D'),
(11, 'KKigveUxNo0', 'L1 A', 'L2 D', 'L3 C'),
(10, 'gyek9zWnCRk', 'L1 E ', 'L2 D', 'L3 C'),
(9, 'YPWQY-39jrI', 'L1 E ', 'L2 C', 'L3 C'),
(8, 'MBXi2_Xe_fo', 'L1 D', 'L2 C', 'L3 C'),
(7, 'PWb5a3iI48Y', 'L1 D', 'L2 C', 'L3 B'),
(6, 'XHXTa9c9Jxc', 'L1 C', 'L2 B', 'L3 B'),
(5, 'Lh4prI_DEi0', 'L1 C', 'L2 B', 'L3 B'),
(4, '6Yl_MNJ4ebc', 'L1 B', 'L2 B', 'L3 A'),
(3, '7uYCKY9VkdE', 'L1 B', 'L2 A', 'L3 A'),
(2, 'IBw-r347dYc', 'L1 A', 'L2 A', 'L3 A'),
(1, 'AE-puazSyP0', 'L1 A', 'L2 A', 'L3 A'),
(34, 'GU9zhIKG3ak', 'L1 E', 'L2 E', 'L3 J');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
